provider "vsphere" {
  user = var.vsphere_user
  password = var.vsphere_password
  vsphere_server = var.vsphere_server

  allow_unverified_ssl = true
}

terraform {
  required_providers {
    nsxt = {
      source = "vmware/nsxt"
    }
  }
}

provider "nsxt" {
    host = var.nsx_manager
    username = var.nsx_username
    password = var.nsx_password
    allow_unverified_ssl = true
    max_retries = 10
    retry_min_delay = 500
    retry_max_delay = 5000
    retry_on_status_codes = [429]
}

data "nsxt_policy_transport_zone" "XXXX" {
    display_name = "XXXX"
}
data "nsxt_policy_transport_zone" "XXXX" {
    display_name = "XXXX"
}

data "nsxt_policy_service" "XXXX" {
    display_name = "XXXX"
}
data "nsxt_policy_service" "XXXX" {
    display_name = "XXXX"
}
data "nsxt_policy_service" "XXXX" {
    display_name = "XXXX"
}
data "nsxt_policy_tier0_gateway" "XXXXXX" {
  display_name = "XXXXX"
}
data "nsxt_policy_tier1_gateway" "XXXXX" {
  display_name = "XXXXXX"
}
data "nsxt_policy_tier1_gateway" "XXXXX" {
  display_name = "XXXXX"
}
data "nsxt_policy_tier1_gateway" "XXXXX" {
  display_name = "XXXXX"
}

data "vsphere_datacenter" "XXXXX" {
    name = "XXXXX"
}
data "vsphere_datastore" "XXXXXXX" {
  name          = "XXXXXXX"
  datacenter_id = "${data.vsphere_datacenter.XXXXX.id}"
}
data "vsphere_datastore" "XXXXX" {
  name = "XXXXXX"
  datacenter_id = "${data.vsphere_datacenter.XXXXXX.id}"
}
data "nsxt_policy_group" "XXXXX" {
   display_name = var.XXXXXX
}
data "nsxt_policy_group" "XXXXXX" {
    display_name = var.XXXXX
}
data "nsxt_policy_group" "XXXXXX" {
    display_name = var.XXXXXX
}
data "nsxt_policy_group" "XXXX" {
    display_name = var.XXXXXX
}


data "nsxt_policy_group" "XXXXX" {
   display_name = var.XXXXXXX
}
data "nsxt_policy_group" "XXXXXX" {
   display_name = var.XXXXXXX
}
data "nsxt_policy_group" "XXXXX" {
   display_name = var.XXXXXX
}
data "nsxt_policy_group" "XXXXXX" {
   display_name = var.XXXXXX
}



data "nsxt_policy_group" "XXXX" {
   display_name = var.XXXXXX
}
data "nsxt_policy_group" "XXXXX" {
   display_name = var.XXXXXX
}
data "nsxt_policy_group" "XXXXX" {
   display_name = var.XXXXXX
}
data "nsxt_policy_group" "XXXXX" {
   display_name = var.XXXXXX
}
data "vsphere_compute_cluster" "XXXX" {
  name          = "XXXX"
  datacenter_id = "${data.vsphere_datacenter.XXXXX.id}"
}
data "nsxt_policy_security_policy" "XXXXXX" {
  is_default = true
  category   = "Application"
}



resource "nsxt_policy_service" "XXXXXX" {
  description  = "XXXXX"
  display_name = var.XXXXXX

  l4_port_set_entry {
    display_name      = "TCP80"
    description       = "TCP port 80 entry"
    protocol          = "TCP"
    destination_ports = ["80"]
  }
}

resource "nsxt_policy_XXXXX" "XXXX" {
    description               = "XXXXX provisioned by Terraform"
    display_name              = "XXXXXX"
    failover_mode             = "NON_PREEMPTIVE"
    default_rule_logging      = "false"
    enable_firewall           = "true"
    enable_standby_relocation = "false"
    tier0_path                = data.nsxt_policy_tierXXXXX.tierX_gw.path
    route_advertisement_types = ["TIERX_STATIC_ROUTES", "TIERX_CONNECTED"]

    tag {
        scope = "XX"
        tag   = "XXXX"
    }

    route_advertisement_rule {
        name                      = "XXXX-X-Networks"
        action                    = "PERMIT"
        subnets                   = [XXXXXXXXXXX]
        prefix_operator           = "GE"
        route_advertisement_types = ["TIERX_CONNECTED"]
    }
}


resource "nsxt_policy_tierX_gateway" "XXXX_gateway" {
    description               = "XXXXX provisioned by Terraform"
    display_name              = "XXXXX"
    failover_mode             = "NON_PREEMPTIVE"
    default_rule_logging      = "false"
    enable_firewall           = "true"
    enable_standby_relocation = "false"
    tier0_path                = data.nsxt_policy_tierXXXXX.tierX_gw.path
    route_advertisement_types = ["TIERX_STATIC_ROUTES", "TIERX_CONNECTED"]

    tag {
        scope = "XX"
        tag   = "XXXXX"
    }

    route_advertisement_rule {
        name                      = "Tier-X-Networks"
        action                    = "PERMIT"
        prefix_operator           = "GE"
        route_advertisement_types = ["TIERX_CONNECTED"]
    }
}

resource "nsxt_policy_tierX_gateway" "tierX_gateway" {
    description               = "XXXXX provisioned by Terraform"
    display_name              = "XXXXX"
    failover_mode             = "NON_PREEMPTIVE"
    default_rule_logging      = "false"
    enable_firewall           = "true"
    enable_standby_relocation = "false"
    tier0_path                = data.nsxt_policy_tierXXXXX.tierX_gw.path
    route_advertisement_types = ["TIER1_STATIC_ROUTES", "TIER1_CONNECTED"]

    tag {
        scope = "XXXX"
        tag   = "XXXXX"
    }

    route_advertisement_rule {
        name                      = "Tier-XX-Networks"
        action                    = "PERMIT"
        prefix_operator           = "GE"
        route_advertisement_types = ["TIERX_CONNECTED"]
    }
}

resource "nsxt_policy_tierX_gateway" "tierX_gateway" {
    description               = "Tier-X provisioned by Terraform"
    display_name              = "XXXX"
    failover_mode             = "NON_PREEMPTIVE"
    default_rule_logging      = "false"
    enable_firewall           = "true"
    enable_standby_relocation = "false"
    tier0_path                = data.nsxt_policy_tierXXXXX.tierX_gw.path
    route_advertisement_types = ["TIER1_STATIC_ROUTES", "TIER1_CONNECTED"]

    tag {
        scope = "XX"
        tag   = "XX"
    }

    route_advertisement_rule {
        name                      = "Tier-X-Networks"
        action                    = "PERMIT"
        prefix_operator           = "GE"
        route_advertisement_types = ["TIERX_CONNECTED"]
    }
}


resource "nsxt_policy_vlan_segment" "XXXX" {
  display_name        = var.XXXX
  description         = "Terraform provisioned Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XX.path
  domain_name         = "XXXXX"
  vlan_ids            = XXXXX
}


resource "nsxt_policy_vlan_segment" "XXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned VLAN Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXXX.path
  domain_name         = "XXXXX"
  vlan_ids            = [XXXXX]

}
  
resource "nsxt_policy_vlan_segment" "XXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned VLAN Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
  domain_name         = "XXXXX"
  vlan_ids            = [XXXXXX]
}
resource "nsxt_policy_vlan_segment" "XXXX" {
  display_name        = var.XXXX
  description         = "Terraform provisioned VLAN Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
  domain_name         = "XXXX"
  vlan_ids            = [XXXXXX]
}
resource "nsxt_policy_vlan_segment" "XXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned VLAN Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
  domain_name         = "XXXX"
  vlan_ids            = [XXXXX]
}
resource "nsxt_policy_vlan_segment" "XXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned VLAN Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
  domain_name         = "XXXX"
  vlan_ids            = [XXXXX]
}
resource "nsxt_policy_vlan_segment" "XXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned VLAN Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
  domain_name         = "XXXX"
  vlan_ids            = [XXXXX]

   subnet {
    cidr        = "XXXXX"
  }
}
resource "nsxt_policy_vlan_segment" "XXXXX" {
  display_name        = var.XXXX
  description         = "Terraform provisioned VLAN Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
  domain_name         = "XXXXX"
  vlan_ids            = [XXXXX]

   subnet {
    cidr        = "XXXXX"
  }
}
resource "nsxt_policy_vlan_segment" "XXXX" {
  display_name        = var.XXXX
  description         = "Terraform provisioned VLAN Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
  domain_name         = "XXXX"
  vlan_ids            = [XXXXX]

   subnet {
    cidr        = "XXXXX"
  }
}
resource "nsxt_policy_vlan_segment" "XXXX" {
  display_name        = var.XXXX
  description         = "Terraform provisioned VLAN Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
  domain_name         = "XXXX"
  vlan_ids            = [X]

   subnet {
    cidr        = "XXXXX"
  }
}
resource "nsxt_policy_segment" "XXXXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXXX.path
  domain_name = "XXXXX"
  vlan_ids = [XXXX]

  subnet {
    cidr        = "XXXX"
  }
}
resource "nsxt_policy_segment" "XXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
}

resource "nsxt_policy_segment" "XXXX" {
  display_name        = var.XXXX
  description         = "Terraform provisioned Segment"
  connectivity_path = nsxt_policy_tier1_gateway.XXXXX.path
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path

  subnet {
    cidr        = "XXXXX"
  }
}
resource "nsxt_policy_segment" "XXXX" {
  display_name        = var.XXXX
  description         = "Terraform provisioned Segment"
  connectivity_path = nsxt_policy_tier1_gateway.XXXXX.path
  transport_zone_path = data.nsxt_policy_transport_zone.XXXXX.path

  subnet {
    cidr        = "XXXXX"
  }
}

resource "nsxt_policy_segment" "XXXX" {
  display_name        = var.XXXX
  description         = "Terraform provisioned Segment"
  connectivity_path = nsxt_policy_tier1_gateway.XXXX.path
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path

  subnet {
    cidr        = "XXXX"
  }
}
resource "nsxt_policy_segment" "XXXX" {
  display_name        = var.XXXX
  description         = "Terraform provisioned Segment"
  connectivity_path = nsxt_policy_tier1_gateway.XXXX.path
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path

  subnet {
    cidr        = "XXXXX"
  }
}
resource "nsxt_policy_segment" "XXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned Segment"
  connectivity_path = nsxt_policy_tier1_gateway.XXXX.path
  transport_zone_path = data.nsxt_policy_transport_zone.XXX.path

  subnet {
    cidr = "XXXX"
  }
}
resource "nsxt_policy_segment" "XXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
}
resource "nsxt_policy_segment" "XXXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
}
resource "nsxt_policy_segment" "XXXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
}
resource "nsxt_policy_segment" "XXXX" {
  display_name        = var.XXXXX
  description         = "Terraform provisioned Segment"
  transport_zone_path = data.nsxt_policy_transport_zone.XXXX.path
}

##Create Security Group

resource "nsxt_policy_group" "XXXX" {
    display_name = var.XXXX
    description  = "Terraform provisioned Group"

    criteria {
        condition {
            key         = "XXXX"
            member_type = "XXXX"
            operator    = "XXXX"
            value       = "XXXX"
        }
    }
}
resource "nsxt_policy_group" "XXXX" {
    display_name = var.XXXX
    description  = "Terraform provisioned Group"

    criteria {
        condition {
            key         = "XXX"
            member_type = "XXXX"
            operator    = "XXXXX"
            value       = "XXXXXX"
        }
    }
}
resource "nsxt_policy_group" "XXXXX" {
    display_name = var.XXXX
    description  = "Terraform provisioned Group"

    criteria {
        condition {
            key         = "XXX"
            member_type = "XXXX"
            operator    = "XXXXX"
            value       = "XXXXX"
        }
    }
}
resource "nsxt_policy_group" "XXXXX" {
    display_name = var.XXXX
    description  = "Terraform provisioned Group"

    criteria {
        condition {
            key         = "XXXX"
            member_type = "XXXX"
            operator    = "XXXX"
            value       = "XXXX"
        }
    }
}


resource "nsxt_policy_group" "XXXXX" {
    display_name = var.XXXXX
    description  = "Terraform provisioned Group"

    criteria {
        condition {
            key         = "XXXX"
            member_type = "XXXX"
            operator    = "XXXX"
            value       = "XXXXX"
        }
    }
}
resource "nsxt_policy_group" "XXXX" {
  display_name = var.XXXXX
  description = "Terraform provisioned Group"

  criteria {
    condition {
      key = "XXXX"
      member_type = "XXX"
      operator = "XXXXX"
      value = "XXXX"
    }
  }
}
resource "nsxt_policy_group" "XXXX" {
  display_name = var.XXXX
  description = "Terraform provisioned Group"

  criteria {
    condition {
      key = "XXXX"
      member_type = "XXXX"
      operator = "XXX"
      value = "XXXX"
    }
  }
}
resource "nsxt_policy_group" "XXXXX" {
  display_name = var.XXXXX
  description = "Terraform provisioned Group"

  criteria {
    condition {
      key = "XXXX"
      member_type = "XXXXX"
      operator = "XXXX"
      value = "XXXX"
    }
  }
}
resource "nsxt_policy_group" "XXXX" {
  display_name = var.XXXX
  description = "Terraform provisioned Group"

  criteria {
    condition {
      key = "X"
      member_type = "XX"
      operator = "XXX"
      value = "XXXX"
    }
  }
}




resource "nsxt_policy_group" "XXXX" {
    display_name = var.XXXX
    description  = "Terraform provisioned Group"

    criteria {
        condition {
            key         = "XXXX"
            member_type = "XXX"
            operator    = "XXXX"
            value       = "XXX"
        }
    }
}
resource "nsxt_policy_group" "XXXX" {
    display_name = var.XXXXX
    description  = "Terraform provisioned Group"

    criteria {
        condition {
            key         = "XXXX"
            member_type = "XXX"
            operator    = "XXXX"
            value       = "XXXX"
        }
    }
}
resource "nsxt_policy_group" "XXX" {
    display_name = var.XXXX
    description  = "Terraform provisioned Group"

    criteria {
        condition {
            key         = "XXXX"
            member_type = "XXXX"
            operator    = "X"
            value       = "XXXX"
        }
    }
}



resource "nsxt_policy_group" "XXXX" {
    display_name = var.XXXX
    description  = "Terraform provisioned Group"

    criteria {
        condition {
            key         = "XXXX"
            member_type = "XXXX"
            operator    = "XXXX"
            value       = "XXXXX"
        }
    }
}


# Create Security Policies
resource"nsxt_policy_security_policy" "XXXXX" {
  display_name = "XXXXX"
    description  = "Terraform provisioned Security Policy"
    category     = "Application"
    locked       = false
    stateful     = true
    tcp_strict   = false
    scope        = [nsxt_policy_group.XXXXX.path]

    rule {
        display_name        = "XXXX"
        destination_groups  = [nsxt_policy_group.XXXX.path]
        action              = "ALLOW"
        services            = [data.nsxt_policy_service.XXX.path]
        logged              = true
        scope               = [nsxt_policy_group.XXXX.path]
    }

    rule {
        display_name        = "XXXXX"
        destination_groups  = [nsxt_policy_group.XXXXX.path]
        action              = "ALLOW"
        services            = [data.nsxt_policy_service.XXXX.path]
        logged              = true
        scope               = [nsxt_policy_group.XXXX.path]
    }


    rule {
        display_name        = "XXXX"
        action              = "ALLOW"
        services            = [data.nsxt_policy_service.XXXX.path]
        logged              = true
        scope               = [nsxt_policy_group.XXXX.path]
    }

    rule {
        display_name        = "Any Deny"
        action              = "REJECT"
        logged              = false
        scope               = [nsxt_policy_group.XXXXX.path]
    }
}

resource "nsxt_policy_security_policy" "XXX" {
  display_name = "XXXX"
  description  = "Terraform provisioned Security Policy"
  category     = "Application"
  locked       = false
  stateful     = true
  tcp_strict   = false

  rule {
    display_name       = "XXXX"
    description        = "In going rule"
    action             = "ALLOW"
    logged             = "false"
    ip_version         = "IPV4"
    destination_groups = [nsxt_policy_group.XXX.path]
    services           = [data.nsxt_policy_service.XXXX.path]
  }

  rule {
    display_name       = "XXXX"
    description        = "In going rule"
    action             = "ALLOW"
    logged             = "false"
    ip_version         = "IPV4"
    destination_groups = [nsxt_policy_group.XXXX.path]
    services           = [data.nsxt_policy_service.XXXX.path]
  }
  rule {
    display_name       = "XXXX"
    description        = "In going rule"
    action             = "ALLOW"
    logged             = "false"
    ip_version         = "IPV4"
    destination_groups = [nsxt_policy_group.XXXX.path]
    services           = [data.nsxt_policy_service.XXXX.path]
  }
  rule {
    display_name       = "XXXX"
    description        = "In going rule"
    action             = "ALLOW"
    logged             = "false"
    ip_version         = "IPV4"
    destination_groups = [nsxt_policy_group.XXXX.path]
    services           = [data.nsxt_policy_service.XXXX.path]
  }

  rule {
    display_name       = "XXXX"
    description        = "In going rule"
    action             = "ALLOW"
    logged             = "false"
    ip_version         = "IPV4"
    destination_groups = [nsxt_policy_group.XXXX.path]
    services           = [data.nsxt_policy_service.XXXX.path]
  }
  rule {
    display_name       = "XXXX"
    description        = "In going rule"
    action             = "ALLOW"
    logged             = "false"
    ip_version         = "IPV4"
    destination_groups = [nsxt_policy_group.XXXX.path]
    services           = [data.nsxt_policy_service.XXXX.path]
  }


  rule {
    display_name = "Deny ANY"
    description  = "Default Deny the traffic"
    action       = "REJECT"
    logged       = "true"
    ip_version   = "IPV4"
  }
}


resource "nsxt_policy_security_policy" "XXXXX" {
    display_name = "XXXX"
    description  = "Terraform provisioned Security Policy"
    category     = "Application"
    locked       = false
    stateful     = true
    tcp_strict   = false
    scope        = [nsxt_policy_group.XXXX.path]

    rule {
        display_name        = "XXXX"
        destination_groups  = [nsxt_policy_group.XXXXX.path]
        action              = "ALLOW"
        services            = [data.nsxt_policy_service.XXXX.path]
        logged              = true
        scope               = [nsxt_policy_group.XXXX.path]
    }

    rule {
        display_name        = "XXXXX"
        destination_groups  = [nsxt_policy_group.XXXX.path]
        action              = "ALLOW"
        services            = [data.nsxt_policy_service.XXXX.path]
        logged              = true
        scope               = [nsxt_policy_group.XXXX.path]
    }


    rule {
        display_name        = "XXXXX "
        destination_groups  = [nsxt_policy_group.XXXX.path]
        action              = "ALLOW"
        services            = [data.nsxt_policy_service.XXXX.path]
        logged              = true
        scope               = [nsxt_policy_group.XXXX.path]
    }

    rule {
        display_name        = "Any Deny"
        action              = "REJECT"
        logged              = false
        scope               = [nsxt_policy_group.XXXX.path]
    }
}


resource "nsxt_policy_service" "XXXX" {
    description  = "HTTP service provisioned by Terraform"
    display_name = "XXXX"

    l4_port_set_entry {
        display_name      = "XXXX"
        description       = "XXXXX"
        protocol          = "XXXX"
        destination_ports = [ "XXXX" ]
    }

    tag {
        scope = "X"
        tag   = "XX"
    }
}


