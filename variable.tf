#variables

#vCenter
variable "vsphere_server" {
    default = "XXXXXXXXXX"
}
#Username & Password for vCenter
variable "vsphere_user" {
  default = "XXXXXXXXXX"
}
variable "vsphere_password" {
    default = "XXXXXXXXXX"
}
#NSX Manager
variable "nsx_manager" {
    default = "XXXXXXXXXX"
}
# Username & Password for NSX-T Manager
variable "nsx_username" {
    default = "XXXXXXX"  
}
variable "nsx_password" {
  default = "XXXXXXXXXX”
}
#Segment Names
variable "nsx_segment_vlanX" {
  default = "TF-Segment-vlanX"
}
variable "nsx_segment_overlay_XXXXX" {
  default = "TF-Segment-XXXXXX"
}


#Security Group Names
variable "nsx_group_XXXXXX" {
    default = "XXXXX"
}

variable "nsx_group_XXXXX" {
    default = "XXXXXX"
}

variable "nsx_group_XXXXXX" {
    default = "XXXXXXXX"
}
